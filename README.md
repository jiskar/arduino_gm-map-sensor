# README #


### What is this repository for? ###

* This is an arduino sketch for reading out analog pressure values from a GM MAP sensor. 
* Version 0

### How do I get set up? ###

connection:
The sensor has a 3-pin connector. Roughly like this:
       +--------+            
       |1   2  3|            
+------+--------+        
|                     |            
|   0                |            
|                     |            
+---------------+            
pin connections
0: air pressure connector
1: 5V
2: analog out (connect to arduino A0)
3. Gnd

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact