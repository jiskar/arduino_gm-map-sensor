#include <LcdBarGraph.h>
#include <Wire.h>
#include <LiquidCrystal.h>

byte lcdNumCols = 16; // -- number of columns in the LCD
float gain = 19, bias = 10; //calibration for 1 bar GM MAP sensor
//float gain = 39.8, bias = 8.8; //calibration for 2 bar GM MAP sensor
//float gain = 62.28, bias = 3.6; //calibration for 3 bar GM MAP sensor
int topOfScale = 966; //formatting option for the bargraph. The values correspond to the raw sensor value (0..1024). set to 1024 if you want full scale
int bottomOfScale = 886; //formatting option for the bargraph. The values correspond to the raw sensor value (0..1024). set to 0 if you want full scale

LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );
LcdBarGraph lbg(&lcd, lcdNumCols,0,1);  // -- creating bargraph instance

void setup()
{
  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("V:xxxx P:xxx kPa");
  Serial.begin(9600);
}

void loop()
{
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5.0 / 1023.0);
  //calculate absolute pressure:
  int pressure = round(voltage * gain + bias);
  
  // print values to terminal:
  Serial.print("voltage: ");
  Serial.print(voltage);
  Serial.print("V - pressure: ");
  Serial.print(pressure);
  Serial.println("kPa");
  
  //print values to lcd:
  int scaleValue = sensorValue - bottomOfScale;  //bias the value for the bargraph
  scaleValue = max(scaleValue,0); // make sure scale doesn't go below zero because this will mess up the display
  lbg.drawValue(scaleValue,topOfScale - bottomOfScale); //max scale = 1024 - the amount removed from the bottom part
  lcd.setCursor(2,0);
  lcd.print(voltage);
  lcd.setCursor(9,0);
  lcd.print(pressure);
  lcd.print(" ");
  delay(10);
}
